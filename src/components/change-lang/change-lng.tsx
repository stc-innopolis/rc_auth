import React from 'react';
import i18next from 'i18next';
import styled from '@emotion/styled';
import { useTranslation } from 'react-i18next';

import { flags } from '../../assets';

const FLAG_WIDTH = 24;

const Wrapper = styled.button`
    border: none;
    background: transparent;
    width: ${FLAG_WIDTH}px;
`;

export const ChangeLng = () => {
    const { i18n } = useTranslation()

    function handleClick() {
        i18n.changeLanguage(i18next.language === 'en' ? 'ru' : 'en')
    }

    return (
        <Wrapper onClick={handleClick}>
            <img
                width={FLAG_WIDTH}
                src={i18next.language === 'en' ? flags.rus : flags.en}
            />
        </Wrapper>
    )
}

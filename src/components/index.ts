import styled from '@emotion/styled';
import { Link as ConnectedLink } from 'react-router-dom';

export { ChangeLng } from './change-lang';

import { authPromoBg } from '../assets';

export const Link = styled(ConnectedLink)`
    display: block;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: #EB3AEF;
    text-align: left;
`;

export const ExternalLink = styled.a`
    display: block;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: #EB3AEF;
    text-align: left;
`;

export const Wrapper = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const Content = styled.div`
    flex-grow: 1;
    max-width: 760px;
    height: 100%;
    max-height: 760px;
    display: flex;
    border-radius: 9px;
    overflow: hidden;
    box-shadow: 0px 4px 14px rgba(0, 0, 0, 0.12);
`;

export const FormPart = styled.div`
    position: relative;
    max-width: 380px;
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    padding: 24px;

    @media (max-width: 780px) {
        max-width: 100%;
    }
`;

export const PromoPart = styled.div`
    background-image: url(${authPromoBg});
    flex-grow: 1;
    max-width: 380px;

    @media (max-width: 780px) {
        display: none;
    }
`;

export const Red = styled.span`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 28px;
    color: #EC5E5E;
    padding-right: 8px;
`;

export const Coder = styled.span`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 28px;
`;

export const FormWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 214px;
    gap: 24px;
`;

export const Header = styled.h3`
    text-align: center;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 28px;
    margin: 24px 0;
`;

export const Error = styled.div(({ theme: { colors } }) => `
    background-color: ${colors.error.light};
    padding: 12px;
    border-radius: 12px;
    border: 1px solid ${colors.error.main};
    color: ${colors.error.main}
`);

export const LoaderWrapper = styled.div`
    position: absolute;
    background: #a3a3a370;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;

    > div {
        height: 0;
    }
`;
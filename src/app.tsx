import React from 'react';
import { Router } from 'react-router-dom';
import { ThemeProvider } from '@emotion/react';
import { Provider } from 'react-redux';
import { getHistory } from '@ijl/cli';
import { themes } from '@ijl/red-coder-ui';

import { Dashboard } from './dashboard';
import { store } from './__data__/store';

import './style';

const history = getHistory();

const App = () => {
    return(
        <Provider store={store}>
            <ThemeProvider theme={themes.lightTheme}>
                <Router  location={history.location} navigator={history}>
                    <Dashboard />
                </Router>
            </ThemeProvider>
        </Provider>
    )
}

export default App;


import { lazy } from 'react';

export const SignInPage = lazy(() => import(/* webpackChunkName: "sign-in-page" */ './sign-in'));

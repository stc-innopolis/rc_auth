import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { getNavigationsValue } from '@ijl/cli';
import { Form, Field } from 'react-final-form';
import { Button, TextInput } from '@ijl/red-coder-ui';

import { URLs } from '../../__data__/urls';
import * as selectors from '../../__data__/selectors';
import {
    Wrapper,
    Content,
    FormPart,
    PromoPart,
    Red,
    Coder,
    Form as StyledForm,
    FormWrapper,
    Header,
    LoaderWrapper,
    Error,
    ExternalLink,
    ChangeLng
} from '../../components';
import { PageLoader } from '../../components/page-loader';
import { redirectFeature } from '../../__data__/features';
import { signIn as signInAction } from '../../__data__/actions/sign-in';

const Page = () => {
    const dispatch = useDispatch()
    const loading = useSelector(selectors.signIn.signInIsLoading);
    const data = useSelector(selectors.signIn.signInData);
    const error = useSelector(selectors.signIn.signInError);
    const navigate = useNavigate();
    const { t } = useTranslation();

    useEffect(() => {
        if (data?.token) {
            localStorage.setItem('red-coder-token', data?.token);
            navigate(getNavigationsValue(redirectFeature.value))
        }
    }, [data])

    const onSubmit = ({ login, password }) => {
        dispatch(signInAction({ login, password }) as any)
    }

    return (
        <Wrapper>
            <Content>
                <FormPart>
                    <div>
                        <Red>RED</Red>
                        <Coder>CODER</Coder>
                    </div>
                    {loading && (
                        <LoaderWrapper>
                            <PageLoader />
                        </LoaderWrapper>
                    )}
                    <FormWrapper>
                    <Form
                        onSubmit={onSubmit}
                        render={({ handleSubmit }) => (
                            <StyledForm onSubmit={handleSubmit}>
                                <Header>{t('rc.auth.header')}</Header>
                                {error && (
                                    <Error>
                                        {error}
                                    </Error>
                                )}
                                <Field
                                    name="login"
                                    validate={value => !value && t('rc.auth.error.required')}
                                    render={({ input, meta }) => (
                                        <TextInput
                                            id="login"
                                            label="login"
                                            error={meta.touched && meta.error}
                                            placeholder="login"
                                            {...input}
                                        />
                                    )}
                                />
                                <Field
                                    name="password"
                                    validate={value => !value && t('rc.auth.error.required')}
                                    render={({ input, meta }) => (
                                        <TextInput
                                            id="password"
                                            label="password"
                                            error={meta.touched && meta.error}
                                            placeholder="password"
                                            {...input}
                                            type="password"
                                        />
                                    )}
                                />
                                
                                <ExternalLink
                                    href={URLs.signUp.getUrl()}
                                >
                                    {t('rc.auth.reg.link')}
                                </ExternalLink>

                                <Button loading={loading} color="action" type="submit">
                                    {t('rc.auth.submit')}
                                </Button>
                            </StyledForm>
                        )}
                    />
                    </FormWrapper>
                    <ChangeLng />
                </FormPart>
                <PromoPart id="promo" />
            </Content>
        </Wrapper>
    )
}

export default Page

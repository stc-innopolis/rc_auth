export { default as authPromoBg } from './images/auth_bg.png';
import rus from './images/ru.svg';
import en from './images/en.svg';

export const flags = {
    rus,
    en
}
 
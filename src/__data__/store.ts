import { configureStore } from '@reduxjs/toolkit';

import { reducer as signInReducer } from './slices/sign-in';

export const store = configureStore({
    reducer: {
        signIn: signInReducer
    }
})

export type StoreType = ReturnType<typeof store.getState>

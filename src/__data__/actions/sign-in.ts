import { actions } from '../slices/sign-in';
import { URLs } from '../urls';

export const signIn = ({ login, password }) => async (dispatch) => {
    dispatch(actions.fetch());

    const responce = await fetch(URLs.api.signIn.url, {
        method: "POST",
        body: JSON.stringify({ login, password }),
        headers: {
            'Content-Type': 'application/json',
        }
    });

    if (!responce.ok) {
        try {
            const data = await responce.json();
            dispatch(actions.error(data?.error));
        } catch(error) {
            dispatch(actions.error('Что-то пошло не совсем так'))
        }
    }

    try {
        const answer = await responce.json();

        dispatch(actions.success(answer?.data));
    } catch (error) {
        dispatch(actions.error('Что-то пошло не совсем так'))
    }
}

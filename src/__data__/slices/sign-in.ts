import { createSlice } from '@reduxjs/toolkit';

import { User } from '../model';

type SigninState = {
    loading: boolean;
    data?: {
        token: string;
        user: User;
    },
    error: string;
}

const initialState: SigninState = {
    loading: false,
    data: null,
    error: null,
}

const slice = createSlice({
    name: 'sign-in',
    initialState,
    reducers: {
        fetch(state) {
            state.loading = true;
            state.error = null;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload;
        },
        error(state, action) {
            state.loading = false;
            state.error = action.payload;
        }
    }
});

export const {
    reducer,
    actions
} = slice;

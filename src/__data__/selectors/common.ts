import { createSelector } from '@reduxjs/toolkit';
import { StoreType } from '../store';

export const rootSelector = createSelector((state: StoreType) => state, state => state);

import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './common';

const rootSignInSelector = createSelector(rootSelector, state => state.signIn);

export const signInIsLoading = createSelector(rootSignInSelector, state => state.loading);
export const signInData = createSelector(rootSignInSelector, state => state?.data);
export const signInError = createSelector(rootSignInSelector, state => state.error);

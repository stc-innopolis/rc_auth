import { getNavigations, getNavigationsValue, getConfigValue } from '@ijl/cli';

const baseUrl = getNavigationsValue(`auth.main`);
const navs = getNavigations();
const makeUrl = url => baseUrl + url;
const apiBase = getConfigValue('auth.api');

export const URLs = {
    baseUrl,
    signIn: {
        url: makeUrl(navs['link.auth.sign-in']),
    },
    signUp: {
        getUrl() {
            return `${navs['link.auth.admin.registration']}?deepLink=https://${location.host}${makeUrl(navs['link.auth.sign-in'])}`
        }
    },
    api: {
        signIn: {
            url: `${apiBase}/sign-in`
        },
    }
};

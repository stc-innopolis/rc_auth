export interface User {
  id: string;
  role: string;
  ijlId: string;
  ijlUser: IjlUser;
  regtime: number;
}

interface IjlUser {
  username: string;
  role: string;
  email: string;
}
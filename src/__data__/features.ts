import { getFeatures } from "@ijl/cli";

const features = getFeatures('auth');

export const redirectFeature = features['auth.redirect'];

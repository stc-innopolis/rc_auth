import React, { Suspense } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import { URLs } from './__data__/urls';
import { PageLoader } from './components/page-loader'
import {
    SignInPage,
} from './pages';

const PageWrapper = ({ children }) => (
    <Suspense fallback={<PageLoader />}>
        {children}
    </Suspense>
)

export const Dashboard = () => (
    <Routes>
        <Route
            path={URLs.baseUrl}
            element={<Navigate replace to={URLs.signIn.url} />}
        />
        <Route path={URLs.signIn.url} element={<PageWrapper><SignInPage /></PageWrapper>} />
    </Routes>
);

const router = require('express').Router();

const stubs = {
    login: 'success',
    timer: '3000'
}

const wait = (req, res, next) => setTimeout(next, +stubs.timer);

router.post('/sign-in', wait, (req, res) => {
    res
        .status(/error/i.test(stubs.login) ? 400 : 200)
        .send(require(`../json/${stubs.login}.json`));
});

router.get('/admin/set/:key/:value', (req, res) => {
    const { key, value } = req.params;
    stubs[key] = value

    res.send('')
})

router.get('/admin', (req, res) => {
    res.send(`
        <body>
            <h1>Стабовая админка</h1>

            <section>
                <h2>Auth</h2>

                <ul>
                    <li><button onclick="fetch('/api/admin/set/login/success')">success</button></li>
                    <li><button onclick="fetch('/api/admin/set/login/error')">error</button></li>
                </ul>
            </section>
        </body>
    `)
})

module.exports = router;

/// <reference types="cypress" />

describe('Auth page tests', () => {
    beforeEach(() => {
        cy.visit('/rc_auth')
        cy.request('/api/admin/set/timer/0')
    })
    
    afterEach(() => {
        cy.request('/api/admin/set/timer/3000')
    })

    it('success auth', () => {
        cy.request('/api/admin/set/login/success')
        cy.intercept(
            {
              method: 'POST',
              url: '/api/sign-in',
            },
        ).as('getUsers')
        cy.location().should((location) => {
            expect(location.pathname).to.eq('/rc_auth/sign-in')
        })

        cy.get('input[name=login]').as('login')

        cy.get('button[type=submit]').click()

        cy.get('@login')
            .type('some valid login')
            .should('have.value', 'some valid login')

        cy.get('input[name=password]')
            .type('my strong secret password shhh')
            .should('have.value', 'my strong secret password shhh')

        cy.get('@login').type('{enter}')

        cy.location().should((location) => {
            expect(location.pathname).to.eq('/red-coder')
        })
    })

    it('auth error', () => {
        cy.request('/api/admin/set/login/error')

        cy.get('input[name=login]').as('login')

        cy.get('@login')
            .type('error')
            .should('have.value', 'error')

        cy.get('input[name=password]')
            .type('my strong secret password shhh')
            .should('have.value', 'my strong secret password shhh')

        cy.get('@login').type('{enter}')

        cy.get('form').toMatchImageSnapshot({
            imageConfig: {
              threshold: 0.001,
            },
        })
    })

    it('screens', () => {
        cy.get('#promo').should('be.visible')
        cy.viewport('macbook-15')
        cy.wait(200)
        cy.get('#promo').should('be.visible')
        cy.viewport('macbook-13')
        cy.wait(200)
        cy.get('#promo').should('be.visible')
        cy.viewport('macbook-11')
        cy.wait(200)
        cy.get('#promo').should('be.visible')
        cy.viewport('ipad-2')
        cy.wait(200)
        cy.get('#promo').should('not.be.visible')
        cy.viewport('ipad-mini')
        cy.wait(200)
        cy.get('#promo').should('not.be.visible')
        cy.viewport('iphone-6+')
        cy.wait(200)
        cy.get('#promo').should('not.be.visible')
        cy.viewport('iphone-6')
    })
})

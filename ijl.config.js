const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        'auth.main': '/rc_auth',
        'link.auth.sign-in': '/sign-in',
        'link.auth.admin.registration': 'https://admin.inno-js.ru/sign-up',
        'main.main': '/red-coder',
    },
    features: {
        'auth': {
            "auth.redirect": {
                "on": true,
                "value": "main.main",
                "key": "auth.redirect"
            }
        },
    },
    config: {
        'red-coder': '/api',
        'auth.api': '/api',
    }
}
